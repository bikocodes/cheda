import pytest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FireFoxService
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions

from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager

from rest_framework.test import APIClient


@pytest.fixture(params=["chrome", "firefox"], scope="class")
def driver_init(request):
    if request.param == "chrome":
        options = ChromeOptions()
        options.add_argument("--headless")
        web_driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()), 
            options=options
        )
    if request.param == "firefox":
        options = FirefoxOptions()
        options.add_argument("--headless")
        web_driver = webdriver.Firefox(
            service=FireFoxService(executable_path=GeckoDriverManager().install()), 
            options=options
        )
    request.cls.driver = web_driver
    yield
    web_driver.close()

@pytest.fixture
def client():
    return APIClient()
 