# cheda

[![Build Status](https://travis-ci.org/The owner of the repository. Either your github username or organization name./cheda.svg?branch=master)](https://travis-ci.org/The owner of the repository. Either your github username or organization name./cheda)
[![Built with](https://img.shields.io/badge/Built_with-Cookiecutter_Django_Rest-F7B633.svg)](https://github.com/agconti/cookiecutter-django-rest)

cheda platform connects end users to reliable cryptpto and stock exachange reliable and legitimate trading platfroms with respective credible stock trading brokerage firms in Africa. It's a home to build and grow your financial muscles.. Check out the project's documentation.. Check out the project's [documentation](http://The owner of the repository. Either your github username or organization name..github.io/cheda/).

# Prerequisites

- [Docker](https://docs.docker.com/docker-for-mac/install/)

# Initialize the project

Start the dev server for local development:

```bash
docker-compose up
```

Create a superuser to login to the admin:

```bash
docker-compose run --rm web ./manage.py createsuperuser
```
