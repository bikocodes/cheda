from os import name
from OpenSSL import crypto
from django.urls import  path
from .views import CryptoView

app_name = 'crypto_app'

urlpatterns = [
    path('crypto/', CryptoView.as_view(), name="crypto_list")
]