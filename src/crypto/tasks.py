import os
import requests
from .models import Coin
from django.forms.models import model_to_dict
from celery import shared_task

@shared_task
def staging_cryptos():
    url = os.getenv("CRYPTO_API")

    data = requests.get(url).json()
    print(f"DATA {data}")
    cryptos = []

    for coin in data:
        obj, created = Coin.objects.get_or_create(symbol=coin["symbol"])

        obj.name = coin["name"]
        obj.symbol = coin["symbol"]

        if obj.price > coin["current_price"]:
            state = "fall"

        elif obj.price == coin["current_price"]:
            state = "same"
        
        elif obj.price < coin["current_price"]:
            state = "raise"

        obj.price = coin["current_price"]
        obj.rank = coin["market_cap_rank"]
        obj.image = coin["image"]

        new_data = model_to_dict(obj)
        new_data.update({"state": state})

        cryptos.append(new_data)

    
