from django.contrib import admin

from src.crypto.models import Coin

admin.site.register(Coin)
