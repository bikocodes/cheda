from rest_framework import serializers

from .models import Coin

class CoinSerializers(serializers.ModelSerializer):
    class Meta:
        model = Coin
        fields = '__all__'