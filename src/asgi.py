import os

from django.core.asgi import get_asgi_application
from django.urls import path

from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack

from src.crypto.consumers import CoinsConsumer
from src.defi.consumers import DefiConsumer
from src.exchange_rates.consumers import CurrencyExchangeRatesConsumer
from src.stock_market.consumers import StockPricesConsumer


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "src.config")

application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": AuthMiddlewareStack(
            URLRouter(
                [   
                    path("ws/coins/", CoinsConsumer.as_asgi()),
                    path("ws/defis/", DefiConsumer.as_asgi()),
                    path("ws/currencies/", CurrencyExchangeRatesConsumer.as_asgi()),
                    path("ws/stocks/", StockPricesConsumer.as_asgi())
                ]
            )
        ),
    }
)
