from rest_framework import serializers
from .models import Trader


class TraderUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trader
        fields = (
            "id",
            "email",
            "user_name",
            "first_name",
            "mobile_number"
        )
        read_only_fields = ("user_name",)


class CreateTraderUserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        user = Trader.objects.create_user(**validated_data)
        return user

    class Meta:
        model = Trader
        fields = (
            "trader_id",
            "user_name",
            "password",
            "first_name",
            "email",
            "mobile_number",
            "auth_token",
        )
        read_only_fields = ("auth_token",)
        extra_kwargs = {
            "password": {"write_only": True}
        }
