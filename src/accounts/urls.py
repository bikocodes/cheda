from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import TraderUserViewSet, CreateTraderUserViewSet

app_name="accounts_app"

router = DefaultRouter()
router.register(r"accounts", TraderUserViewSet)
router.register(r"accounts", CreateTraderUserViewSet)

urlpatterns = [
    path("api/v1/", include(router.urls))
] 