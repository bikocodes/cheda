from rest_framework import viewsets, mixins
from rest_framework.permissions import AllowAny
from .models import Trader
from .permissions import IsUserOrReadOnly
from .serializers import TraderUserSerializer, CreateTraderUserSerializer


class TraderUserViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):

    queryset = Trader.objects.all()
    serializer_class = TraderUserSerializer
    permission_classes = (IsUserOrReadOnly,)


class CreateTraderUserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Trader.objects.all()
    serializer_class = CreateTraderUserSerializer
    permission_classes = (AllowAny,)
