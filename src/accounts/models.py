from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.db import transaction

from django.contrib.auth.models import (
    BaseUserManager,
    AbstractBaseUser,
    PermissionsMixin,
)


class CustomAccountManager(BaseUserManager):
    def create_user(self, email, user_name, first_name, password, **other_fields):

        if not email:
            raise ValueError(_("You must provide an email address"))

        email = self.normalize_email(email)
        user = self.model(
            email=email, user_name=user_name, first_name=first_name, **other_fields
        )
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, user_name, first_name, password, **other_fields):

        other_fields.setdefault("is_staff", True)
        other_fields.setdefault("is_superuser", True)
        other_fields.setdefault("is_active", True)

        if other_fields.get("is_staff") is not True:
            raise ValueError("Superuser must be assigned to is_staff=True.")
        if other_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must be assigned to is_superuser=True.")

        return self.create_user(email, user_name, first_name, password, **other_fields)


class ChedaUser(AbstractBaseUser, PermissionsMixin):
    class Role(models.TextChoices):
        ADMIN = "ADMIN", "Admin"
        TRADER = "TRADER", "Trader"

    base_role = Role.ADMIN
    role = models.CharField(_("Role"), max_length=50, choices=Role.choices)

    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.pk:
            self.role = self.base_role
            return super().save(*args, **kwargs)

    email = models.EmailField(_("email address"), unique=True)
    user_name = models.CharField(max_length=150, unique=True)
    first_name = models.CharField(max_length=150)
    start_date = models.DateTimeField(default=timezone.now)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    objects = CustomAccountManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["user_name", "mobile_number"]

    def __str__(self):
        return self.user_name


class TraderManager(CustomAccountManager):
    def get_queryset(self, *args, **kwargs):
        results = super().get_queryset(*args, **kwargs)
        return results.filter(role=ChedaUser.Role.TRADER)


class Trader(ChedaUser):
    base_role = ChedaUser.Role.TRADER
    objects = TraderManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.pk:
            self.role = ChedaUser.Role.TRADER
        return super().save(*args, **kwargs)


class ChedarAdmin(ChedaUser):
    base_role = ChedaUser.Role.ADMIN
    objects = TraderManager()

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.pk:
            self.role = ChedaUser.Role.ADMIN
        return super().save(*args, **kwargs)


class TraderProfile(models.Model):
    user = models.OneToOneField(ChedaUser, on_delete=models.CASCADE)
    mobile_number = models.CharField(max_length=150, blank=False, null=False)
    trader_id = models.IntegerField(null=False, blank=False, primary_key=True)

    def __str__(self):
        return self.ChedaUser.user_name
