from .local import Local  # noqa
from .production import Production  # noqa
from .celery import app as celery_app

__all__ = ("celery_app",)
