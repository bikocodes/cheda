import os
from celery import Celery
from configurations import importer

importer.install()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.common")

app = Celery("common")

app.config_from_object("django.conf:common", namespace="CELERY")

app.autodiscover_tasks()

app.conf.beat_schedule = {

    "get_crypto_data_10s": {
        "task": "crypto.tasks.staging_cryptos", 
        "schedule": 10.0
    },

    "get_defi_data_20s": {
        "task": "defi.tasks.staging_defis",
        "schedule": 20.0
    },

    "get_exchange_rates_30s": {
        "task": "exchange_rates.tasks.staging_currencies",
        "schedule": 30.0
    },

    "get_stock_market_40s": {
        "task": "stock_market.tasks.staging_stocks",
        "schedule": 40.0
    }


}
