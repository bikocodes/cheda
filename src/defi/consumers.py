import json

from channels.generic.websocket import AsyncWebsocketConsumer

from .serializers import DefiMarketCapitalizationSerializers
class DefiConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add("defis", self.channel_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard("defis", self.channel_name)

    async def send_new_data(self, event):
        new_data = event["text"]
        serializer = DefiMarketCapitalizationSerializers(new_data)
        await self.send(json.dumps(serializer.data))
