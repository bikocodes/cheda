from os import name
from django.urls import  path
from .views import DefiView

app_name="defi_app"

urlpatterns = [
    path('defi/', DefiView.as_view(), name="defis_list")
]