from rest_framework import serializers

from .models import DefiMarketCapitalization

class DefiMarketCapitalizationSerializers(serializers.ModelSerializer):
    class Meta:
        model = DefiMarketCapitalization
        fields = '__all__' 