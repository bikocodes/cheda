from django.db import models

class DefiMarketCapitalization(models.Model):
    defi_market_cap = models.IntegerField(default=0, blank=False, null=False)
    eth_market_cap = models.IntegerField(default=0, blank=False, null=False)
    defi_eth_ratio = models.FloatField(default=0, blank=False, null=False)
    trading_volume = models.IntegerField(default=0, blank=False, null=False)
    defi_dominance = models.FloatField(default=0, blank=False, null=False)
    defi_leading_coin = models.CharField(max_length=255,default=0,blank=False, null=False)
