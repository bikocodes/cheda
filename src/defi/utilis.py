from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from .models import  DefiMarketCapitalization

channel_layer = get_channel_layer()

def send_defis_to_channels():
    async_to_sync(channel_layer.group_send)(
        "coins", {
            "type": "send_new_data", 
            "text": DefiMarketCapitalization.objects.all()
        }
    )

