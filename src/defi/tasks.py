import os
import requests
from .models import DefiMarketCapitalization
from django.forms.models import model_to_dict
from celery import shared_task


@shared_task
def staging_defis():
    url = os.getenv("DEFI_MARKET_API")

    data = requests.get(url).json()
    print(f"DATA {data}")
    defis = []

    for defi in data:
        obj, created = DefiMarketCapitalization.objects.get_or_create(
            defi[
                "defi_market_cap", "eth_market_cap", "defi_eth_ratio",
                "trading_volume", "defi_dominance", "defi_leading_coin"
            ])
        
        obj.defi_market_cap = defi["defi_market_cap"]
        obj.eth_market_cap = defi["eth_market_cap"]
        obj.defi_eth_ratio = defi["defi_to_eth_ratio"]
        obj.trading_volume = defi["trading_volume_24h"]
        obj.defi_dominance = defi["defi_dominance"]
        obj.defi_leading_coin = defi["top_coin_name"]

        new_data = model_to_dict(obj)

        defis.append(new_data)
    