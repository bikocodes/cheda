from django.contrib import admin

from .models import DefiMarketCapitalization

admin.site.register(DefiMarketCapitalization)