from rest_framework import serializers

from .models import StockPrices

class StockPricesSerializers(serializers.ModelSerializer):
    class Meta:
        model = StockPrices
        fields = '__all__'