import os
import requests
from .models import StockPrices
from django.forms.models import model_to_dict
from celery import shared_task


@shared_task
def staging_stocks():
    url = os.getenv("STOCK_MARKET_API_KEY")

    data = requests.get(url).json()
    print(f"DATA {data}")
    stocks = []

    for stock in data:
        obj, created = StockPrices.objects.get_or_create(symbol=stock["symbol"])

        obj.symbol = stock["symbol"]

        if obj.open_price > stock["high"]:
            state = "fall"

        elif obj.day_low < stock["current_price"]:
            state = "raise"

        elif obj.close_price > stock["open"]:
            state = "raise"

        elif obj.close_price < stock["open"]:
            state = "fall"

        obj.open_price = stock["open"]
        obj.day_high = stock["high"]
        obj.day_low = stock["low"]
        obj.close_price = stock["close"]
        obj.volume = stock["volume"]
        
        new_data = model_to_dict(obj)
        new_data.update({"state": state})

        stocks.append(new_data)
    
