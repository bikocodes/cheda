from os import name
from django.urls import  path
from .views import StockMarketView

app_name="stock_market_app"

urlpatterns = [
    path('stock_market/', StockMarketView.as_view(), name="stocks_lists")
]