from django.db import models

class StockPrices(models.Model):
    symbol = models.CharField(max_length=155)
    open_price = models.FloatField(default=0, blank=False, null=False)
    day_high = models.FloatField(default=0.00, blank=False, null=False)
    day_low = models.FloatField(default=0.00, blank=False, null=False)
    close_price = models.FloatField(default=0, blank=False, null=False)
    volume = models.DecimalField(max_digits=255, decimal_places=2)
    
    def __str__(self):
        return self.symbol
        
    class Meta:
        ordering = ["day_high"]

        