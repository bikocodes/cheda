from django.views.generic import TemplateView

class StockMarketView(TemplateView):
    template_name = "stock_market.html"

