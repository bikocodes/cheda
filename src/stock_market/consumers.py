from channels.generic.websocket import AsyncWebsocketConsumer
import json

from .serializers import StockPricesSerializers


class StockPricesConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add("stocks", self.channel_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard("stocks", self.channel_name)

    async def send_new_data(self, event):
        new_data = event["text"]
        serializer = StockPricesSerializers(new_data)
        await self.send(json.dumps(serializer.data))
