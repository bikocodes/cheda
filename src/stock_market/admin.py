from django.contrib import admin

from .models import StockPrices

admin.site.register(StockPrices)
