from django.conf import settings
from django.urls import path, re_path, include, reverse_lazy
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import RedirectView

from rest_framework.authtoken import views

urlpatterns = [
    path("admin/", admin.site.urls),

    path("api-token-auth/", views.obtain_auth_token),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),

    path("", include("src.accounts.urls", namespace="accounts")),
    path("", include("src.crypto.urls", namespace="crypto_app")),
    path("", include("src.defi.urls", namespace="defi_app")),
    path("", include("src.exchange_rates.urls", namespace="exchange_rates_app")),
    path("", include("src.stock_market.urls", namespace="stock_market_app")),

    re_path(r"^$", RedirectView.as_view(url=reverse_lazy("api-root"), permanent=False)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
