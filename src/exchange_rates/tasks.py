import os
import requests
from .models import CurrencyExachangeRates
from django.forms.models import model_to_dict
from celery import shared_task


@shared_task
def staging_currencies():
    url = os.getenv("CURRENCY_EXCHANGE_RATES")

    data = requests.get(url).json()
    print(f"DATA {data}")
    currencies = []

    for currency in data:
        obj, created = CurrencyExachangeRates.objects.get_or_create(
            currency["symbols", "base_currency", "amount"]
        )

        for k,v in data["rates"].items():
            obj.symbols = currency[k]
            obj.amount = currency[v]
        
        new_data = model_to_dict(obj)

        currencies.append(new_data)
