from django.views.generic import TemplateView

class ExachangeRatesView(TemplateView):
    template_name = "exchange_rates.html"
