from os import name
from django.urls import  path
from .views import ExachangeRatesView

app_name="exchange_rates_app"

urlpatterns = [
    path('exchange_rates/', ExachangeRatesView.as_view(), name="exchange_rates_lists")
]