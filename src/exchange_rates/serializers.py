from rest_framework import serializers

from .models import CurrencyExachangeRates

class CurrencyExachangeRatesSerializers(serializers.ModelSerializer):
    class Meta:
        model = CurrencyExachangeRates
        fields = '__all__'