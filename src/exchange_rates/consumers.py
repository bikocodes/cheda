from channels.generic.websocket import AsyncWebsocketConsumer
import json

from .serializers import CurrencyExachangeRatesSerializers

class CurrencyExchangeRatesConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add("currencies", self.channel_name)
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard("currencies", self.channel_name)

    async def send_new_data(self, event):
        new_data = event["text"]
        serializer = CurrencyExachangeRatesSerializers(new_data)
        await self.send(json.dumps(serializer.data))
