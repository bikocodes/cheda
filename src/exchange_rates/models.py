from django.db import models

class CurrencyExachangeRates(models.Model):
    base_currency = models.CharField(max_length=50, default='USD')
    symbols = models.CharField(max_length=255,blank=True, default='=USD,EUR,KSH')
    amount = models.FloatField(default=0, blank=True)

