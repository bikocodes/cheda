from django.contrib import admin

from .models import CurrencyExachangeRates

admin.site.register(CurrencyExachangeRates)
