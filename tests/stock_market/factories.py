import  factory

from src.stock_market.models import StockPrices

from faker import Faker

faker = Faker()

class StockPricesFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = StockPrices

    symbol = "KPLC"
    open_price = "1.50"
    day_high = "3.50"
    day_low = "1.70"
    close_price = "2.40"
    volume = "150000"

