import  pytest

pytestmark = pytest.mark.django_db

def test_stocks_str(stock):
    assert stock.__str__() == "KPLC"

def test_stocks_day_low(stock):
    assert stock.day_low == "1.70"
    