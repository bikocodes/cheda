import pytest

from django.http import response
from django.urls import reverse

pytestmark = pytest.mark.django_db

def test_exchange_rates_view(client):
    url = reverse("stock_market_app:stocks_lists")
    response = client.get(url)
    assert response.status_code == 200