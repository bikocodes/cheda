import pytest 
from pytest_factoryboy import register

from .factories import StockPricesFactory

register(StockPricesFactory)

def stock(db, stock_prices_factory):
    stock_price = stock_prices_factory.create()
    return stock_price