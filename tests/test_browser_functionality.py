import pytest

@pytest.mark.usefixtures("driver_init")
class Test_URL_BROWSERS:
    def test_open_url(self, live_server):
        self.driver.get(("%s%s" % (live_server.url, "/admin/")))
        assert "Log in | Django site admin" in self.driver.title