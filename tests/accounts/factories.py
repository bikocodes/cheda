from pyexpat import model
import django
import factory

from src.accounts.models import ChedarAdmin, Trader, TraderProfile

from faker import  Faker

fake = Faker()

class ChedaAdminFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = ChedarAdmin
    
    base_role = "ADMIN"
    email = "test@admin.com"
    user_name = "BikoAdmin"
    password = "test"
    is_active = True
    is_staff = True

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        if "is_superuser" in kwargs:
            return manager.create_superuser(*args, **kwargs)
        else:
            return manager.create_user(*args, **kwargs)


class TraderFactory(factory.django.DjangoModelFactory):
    
    class Meta:
        model = Trader

    base_role = "TRADER"
    email = "test@user.com"
    user_name = "BikoTrader"
    password = "test"
    is_active = True
    is_staff = False

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        if "is_superuser" in kwargs:
            return manager.create_superuser(*args, **kwargs)
        else:
            return manager.create_user(*args, **kwargs)

class TraderProfileFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = TraderProfile

    user = factory.SubFactory(TraderFactory)
    mobile_number = "254254254"
    trader_id = "1"
    
