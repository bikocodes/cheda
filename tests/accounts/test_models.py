from importlib_metadata import email
import pytest

pytestmark = pytest.mark.django_db

def test_cheda_admin_str(cheda_admin):
    assert cheda_admin.__str__ == "BikoAdmin"

def test_cheda_admin_emailfield_empty(cheda_admin):
    with pytest.raises(ValueError) as e:
        test = cheda_admin.email = ""
    assert str(e.value) == "Admin Account: You must provide an email address"

def test_cheda_admin_email_incorrect(cheda_admin):
    with pytest.raises(ValueError) as e:
        test = cheda_admin(email="biko@biko.com", is_staff=True, is_supperuser=True)
    assert str(e.value) == "Admin Account: You must provide a valid email address"

def test_cheda_admin_not_staff(cheda_admin):
    with pytest.raises(ValueError) as e:
        test = cheda_admin(is_staff=False, is_supperuser=True)
    assert str(e.value) == "Superuser must be assigned to is_staff=True."

def test_cheda_admin_not_superuser(cheda_admin):
    with pytest.raises(ValueError) as e:
        test = cheda_admin(is_staff=True, is_supperuser=False)
    assert str(e.value) == "Superuser must be assigned to is_supperuser=True."

def test_cheda_admin_role(cheda_admin):
    assert cheda_admin.base_role == "ADMIN"

def test_trader_str(trader):
    assert trader.__str__ == "BikoTrader"

def test_trader_emailfield_empty(trader):
    with pytest.raises(ValueError) as e:
        test = trader.email = ""
    assert str(e.value) == "Trader Account: You must provide an email address."

def test_trader_email_incorrect(trader):
    with pytest.raises(ValueError) as e:
        test = trader.email 
    assert str(e.value) == "Trader Account: You must provide a valid email address"

def test_trader_email_correct(trader):
    assert trader.email == "test@user.com"

def test_trader_role(trader):
    assert trader.base_role == "TRADER"

def test_trader_profile_id(trader_profile):
    assert trader_profile.trader_id == "1"

