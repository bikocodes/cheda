import pytest
from pytest_factoryboy import register

from rest_framework.test import APIClient

from .factories import ChedaAdminFactory, TraderFactory, TraderProfileFactory

register(
    ChedaAdminFactory,
    TraderFactory,
    TraderProfileFactory
)


@pytest.fixture
def cheda_admin(db, cheda_admin_factory):
    cheda_admin = cheda_admin_factory.create()
    return cheda_admin

@pytest.fixture
def trader(db, trader_factory):
    trader = trader_factory.create()
    return trader

@pytest.fixture
def trader_profile(db, trader_profile_factory):
    trader_profile = trader_profile_factory.create()
    return trader_profile

@pytest.fixture
def client():
    return APIClient()

