import pytest

from src.crypto.tasks import staging_cryptos
from src.crypto.models import Coin

from unittest.mock import patch, MagicMock

pytestmark = pytest.mark.django_db


@patch('staging_cryptos')
def test_crypto_staging (client, mock_staging_cryptos):
    mock_response = MagicMock(mock_staging_cryptos)
    mock_response.status_code = 200
    mock_response.json.return_value != {}

    client.get.return_value = mock_response

    assert Coin.objects.all() == 0
    assert staging_cryptos() != 0

@patch('staging_cryptos')
def test_fail_crypto_staging(client):
    mock_responses = MagicMock(status_code = 403)
    mock_responses.json.return_value != 0
    client.get.return_value = mock_responses
    
    assert staging_cryptos() == 0
    

