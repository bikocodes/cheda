import factory

from src.crypto.models import Coin

from faker import Faker

fake = Faker()

class CoinFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Coin

    symbol = "BTC"
    name = "Bitcoin"
    price = "13.50"
    rank = "1"
    image = ""
