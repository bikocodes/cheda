import pytest

from django.http import response
from django.urls import reverse

pytestmark = pytest.mark.django_db

def test_crypto_view(client):
    url = reverse("crypto_app:crypto_list")
    response = client.get(url)
    assert response.status_code == 200