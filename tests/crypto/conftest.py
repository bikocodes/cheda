import pytest
from pytest_factoryboy import register

from .factories import CoinFactory

register(CoinFactory)

@pytest.fixture
def coin(db, coin_factory):
    coin = coin_factory.create()
    return coin