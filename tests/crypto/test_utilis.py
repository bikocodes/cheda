import pytest

from src.crypto.utilis import send_coins_to_channels
from src.crypto.models import Coin, Coins

def test_send_data_to_channels():
    send_coins = send_coins_to_channels()
    
    assert send_coins.data != 0
    assert Coin.objects.all() >= 1
