import pytest

from src.crypto.serializers import CoinSerializers


def test_coin_serializer(coin):
    serialized_data = CoinSerializers(coin)

    assert serialized_data.data == coin.data
    assert serialized_data.name == "Bitcoin"

