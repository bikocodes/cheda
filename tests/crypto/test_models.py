import pytest

pytestmark = pytest.mark.django_db

def test_crpto_str(coin):
    assert coin.__str__() == "Bitcoin"

def test_crypto_price(coin):
    assert coin.price == "13.50"

