import pytest

from unittest.mock import patch, MagicMock
from src.crypto.consumers import CoinsConsumer

@patch('CoinsConsumer')
def test_cypto_consumer(mock_coins_consumer):
    send_to_channels = mock_coins_consumer.send_new_data()

    assert send_to_channels.data != 0
    assert send_to_channels.data == {}
    

