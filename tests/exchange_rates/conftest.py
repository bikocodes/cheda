from atexit import register
import pytest
from pytest_factoryboy import pytest

from .factories import CurrencyExachangeRatesFactory

register(CurrencyExachangeRatesFactory)

@pytest.fixture
def currency_rate(db, currency_exchange_rates_factory):
    currency = currency_exchange_rates_factory.create()
    return currency 

    