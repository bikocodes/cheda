import pytest

pytestmark = pytest.mark.django_db

def test_exchange_rate_amount(currency_rate):
    assert currency_rate.amount == "100000"
