import factory

from src.exchange_rates.models import  CurrencyExachangeRates

from faker import Faker

class CurrencyExachangeRatesFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = CurrencyExachangeRates

    base_currency = "USD"
    symbols = "KES, EUR"
    amount = "100000"