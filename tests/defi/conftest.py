import pytest
from pytest_factoryboy import  register

from .factories import DefiMarketCapitalizationFactory

register(
    DefiMarketCapitalizationFactory
)

@pytest.fixture
def defi(db, defi_market_capitalization_factory):
    defi = defi_market_capitalization_factory.create()
    return defi
