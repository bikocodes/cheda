import pytest

pytestmark = pytest.mark.django_db

def test_defi_market_capitalization(defi):
    assert defi.defi_market_cap == "100000"
