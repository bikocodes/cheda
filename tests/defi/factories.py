from pyexpat import model
import factory

from src.defi.models import DefiMarketCapitalization

from faker import Faker

class DefiMarketCapitalizationFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = DefiMarketCapitalization

    defi_market_cap = "100000"
    eth_market_cap = "200000"
    defi_eth_ratio = "50.50"
    trading_volume = "300000"
    defi_dominance = "60.50"
    defi_leading_coin = "DAI"


