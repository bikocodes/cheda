import pytest

from django.http import response
from django.urls import reverse

pytestmark = pytest.mark.django_db

def test_crypto_view(client):
    url = reverse("defi_app:defis_list")
    response = client.get(url)
    assert response.status_code == 200