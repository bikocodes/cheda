## Table of contents

* [About](#about)

* [Acknowledgements](#acknowledgements)

* [API Reference](#api-reference)

* [Features](#features)

* [Tech Stack](#tech-stack)

* [Local Development](#local-development)

* [Running Tests](#running-tests)

* [Demo](#demo)

* [Lessons Learned](#lessons-learned)

* [License](#license)

# About

**Version 1.0.0**

CHEDA platform is aminiature software program that connects end users to reliable global stocks, degital currencies, fiat exchange rates and DeFi market convergence. It is a distributed realtime application that enables users to learn how to trade and invest with dummy KES amount in a traders account as per their own picking.


## Tech Stack

- Python 3.8.10
- Django 4.0.1
- Django Rest Framework 3.13.1
- Django Jinja Templating
- Celery
- Channels
- Redis
- Websocket
- HTML5
- CSS
- Vuejs
- Selenium 4.1.0
- Docker
- Ansible

## Lessons Learned

- How to use REDIS as a message broker and the creations of application respctive tasks.
- How CELERY is used in scheduling tasks execution at a specific time on the api calls.
- Celery is loaded automatically when Django applications start. The beat is used as a 
  celery scheduler
- A broker is a task que that stores our task(s) data stracture after a broker gets a 
  tasks from the django app it puts the task in a que and starts to pass the tasks to 
  workers.

## License

[MIT](https://choosealicense.com/licenses/mit/)